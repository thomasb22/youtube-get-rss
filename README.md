# youtube-get-rss
Outil permettant de retrouver le lien RSS d'une chaîne Youtube.
## Licence
Le code est sous licence [AGPL-3.0](https://www.gnu.org/licenses/agpl.html)
## Démonstration
Vous trouverez une démo à cette adresse : https://thomasblanschong.fr/youtube-get-rss/
