<?php
include('functions.php');

if (isset($_GET['s'])) {
	$search = urlencode($_GET['s']);
	$searchYoutube = 'https://www.youtube.com/results?filters=channel&search_query='.$search.'&lclk=channel&gl=FR&hl=fr';

	$results = parseYoutubeSearch($searchYoutube);
}
else {
	$search = '';
	$results = array();
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="Outil permettant de retrouver le lien RSS d'une chaîne ou d'une playlist Youtube." />
		<link rel="apple-touch-icon" sizes="57x57" href="img/apple-icon-57x57.png" />
		<link rel="apple-touch-icon" sizes="60x60" href="img/apple-icon-60x60.png" />
		<link rel="apple-touch-icon" sizes="72x72" href="img/apple-icon-72x72.png" />
		<link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon-76x76.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="img/apple-icon-114x114.png" />
		<link rel="apple-touch-icon" sizes="120x120" href="img/apple-icon-120x120.png" />
		<link rel="apple-touch-icon" sizes="144x144" href="img/apple-icon-144x144.png" />
		<link rel="apple-touch-icon" sizes="152x152" href="img/apple-icon-152x152.png" />
		<link rel="apple-touch-icon" sizes="180x180" href="img/apple-icon-180x180.png" />
		<link rel="icon" type="image/png" sizes="192x192"  href="img/android-icon-192x192.png" />
		<link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png" />
		<link rel="icon" type="image/png" sizes="96x96" href="img/favicon-96x96.png" />
		<link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png" />
		<link rel="manifest" href="manifest.json" />
		<meta name="msapplication-TileColor" content="#ffffff" />
		<meta name="msapplication-TileImage" content="img/ms-icon-144x144.png" />
		<meta name="theme-color" content="#ffffff" />
		<link rel="search" type="application/opensearchdescription+xml" href="opensearch.php" title="Youtube Get RSS" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<title>Youtube Get RSS</title>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<img src="img/logo.svg" alt="Youtube Get RSS" class="col-xs-10 col-xs-offset-1" />
			</div>
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 formsearch">
					<form class="input-group col-lg-12" action="" method="get">
						<input type="search" class="form-control" name="s" placeholder="Rechercher…" value="<?php echo urldecode($search) ?>" required />
						<span class="input-group-btn">
							<button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span></button>
						</span>
					</form>
				</div>
			</div>
<?php
showResults($results);
?>
		</div>
		<footer class="container text-center">
			<a href="https://framagit.org/thomasb22/youtube-get-rss" target="_blank">Sources disponibles sur Framagit sous licence AGPL</a>
		</footer>
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>
