<?php
if (empty($_SERVER['HTTPS']))
	$port = 'http';
else
	$port = 'https';

echo '<?xml version="1.0" encoding="UTF-8"?>
<OpenSearchDescription xmlns="http://a9.com/-/spec/opensearch/1.1/">
	<ShortName>Youtube Get RSS</ShortName>
	<LongName>Youtube Get RSS</LongName>
	<Description>Rechercher le flux RSS d\'une chaîne Youtube.</Description>
	<InputEncoding>UTF-8</InputEncoding>
	<OutputEncoding>UTF-8</OutputEncoding>
	<AdultContent>false</AdultContent>
	<Language>fr-FR</Language>
	<Developer>Thomas Blanschong</Developer>
	<Contact>contact@thomasblanschong.fr</Contact>
	<Tags>youtube rss atom xml</Tags>
	<Attribution>Rechercher le flux RSS d\'une chaîne Youtube.</Attribution>
	<SyndicationRight>open</SyndicationRight>
	<Query role="example" searchTerms="gnu+linux"/>
	<Image height="16" width="16" type="image/vnd.microsoft.icon">'.$port.'://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']).'/img/favicon.ico</Image>
	<Image height="64" width="64" type="image/png">'.$port.'://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']).'/img/favicon-64x64.png</Image>
	<Url type="text/html" template="'.$port.'://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['SCRIPT_NAME']).'/?s={searchTerms}"/>
</OpenSearchDescription>';
?>
