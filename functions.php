<?php
function parseYoutubeSearch($urlYoutube) {
	$i = 0;
	$check = false;
	$html = new DOMDocument;
	$html->loadHTMLFile($urlYoutube);
	$html->encoding = 'UTF-8';
	$html->saveHTML();
	$xpath = new DOMXPath($html);
	$items = $xpath->query('//ol[contains(@class,"item-section")] //li');
	foreach ($items as $item) {
		foreach ($item->getElementsByTagName('a') as $links) {
			if (!$check)
				$check = true;
			else
				break;
			$results[$i]['channelURL'] = 'https://www.youtube.com'.$links->getAttribute('href');
			$results[$i]['rssURL'] = 'https://www.youtube.com/feeds/videos.xml?channel_id='.$links->getAttribute('data-ytid');
		}
		foreach ($item->getElementsByTagName('h3') as $title)
			$results[$i]['title'] = utf8_decode($title->nodeValue);
		foreach ($item->getElementsByTagName('img') as $imgs) {
			if (!preg_match('#.+\.gif#i', $imgs->getAttribute('src')))
				$results[$i]['imgURL'] = $imgs->getAttribute('src');
			else
				$results[$i]['imgURL'] = $imgs->getAttribute('data-thumb');
			$results[$i]['imgAlt'] = utf8_decode($results[$i]['title']);
		}
		$check = false;
		if (!empty($links->getAttribute('data-ytid')))
			$i++;
	}

	return $results;
}

function showResults($results) {
	foreach ($results as $result) {
?>
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<img width="110" height="110" src="<?php echo $result['imgURL'] ?>" alt="<?php echo $result['imgAlt'] ?>" class="pull-left avatar" />
					<a href="<?php echo $result['channelURL'] ?>" target="_blank"><?php echo $result['title'] ?></a>
					<div class="input-group">
						<span class="input-group-addon rss"><i class="fa fa-rss fa-lg"></i></span>
						<input type="url" class="form-control rss2" placeholder="Lien RSS…" value="<?php echo $result['rssURL'] ?>" onclick="this.focus();this.select()" readonly />
					</div>
				</div>
			</div>
<?php
	}
}
?>
